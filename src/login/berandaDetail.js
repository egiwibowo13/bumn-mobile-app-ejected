import React from "react";
import { AppRegistry, View, StyleSheet, StatusBar, Image } from "react-native";
import {
    Button,
    Text,
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Header,
    Left,
    Badge,
    Right,
    Icon,
    Title,
    Input,
    InputGroup,
    Item,
    Tab,
    Tabs,
    Footer,
    FooterTab,
    Label,
    Thumbnail,
    ListItem,
    CheckBox,
    Picker,
    TouchableOpacity
} from "native-base";


import { responsiveHeight, responsiveWidth, responsiveFontSize } from 'react-native-responsive-dimensions';


export default class BerandaDetail extends React.Component {



    constructor(props) {
        super(props);
        this.state = {
            selected: "key0",
            status: ""
        };
    }

    ShowHideTextComponentView = () => {

        if (this.state.status == "key1") {
            this.setState({ status: false })
        }
        else {
            this.setState({ status: true })
        }
    }

    onValueChange3(value) {
        this.setState({
            selected: value
        });
        if (this.state.selected == "key1") {
            this.setState({ status: false })
        }
        else {
            this.setState({ status: true })
        }
    }

    onClickTeruskan() {
        
       
    }


    // componentDidMount() {
    //     fetch("https://penyewaanbatch124.herokuapp.com/api/barang/" + this.state.idBarang, {
    //         method: "GET"
    //     })
    //         .then((response) => response.json())
    //         .then((data) => {

    //             this.setState({
    //                 dataBarang: data,
    //                 jumlahBarang: data.JumlahBarang
    //             });

    //             console.log(this.state.dataBarang.NamaBarang);
    //         })
    //         .catch((error) => {
    //             console.log(error);
    //         })
    // }

    render() {
        return (
            <Container style={{  }}>
                <Header style={{ backgroundColor: "#fff" }}>
                    <Left>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate("Beranda")}
                        >
                            <Icon name="arrow-back" />
                        </Button>
                    </Left>
                    <Body style={{ backgroundColor: "#fff", alignItems: "center" }}>
                        <Title style={{ color: "#000" }}>Detail Laporan</Title>
                    </Body>
                    <Right />
                </Header>
                <Content>
                    <Image source={require("../../img/asset/imagedrag.png")} style={{ height: 200, width: null, flex: 1 }} />
                    <Card>

                        <CardItem cardBody>

                        </CardItem>

                        <CardItem>
                            <Left>
                                <Thumbnail small source={require("../../img/asset/imagedrag.png")} />
                                <Body>
                                    <Text>NativeBase</Text>
                                    <Text note>GeekyAnts</Text>
                                </Body>
                            </Left>
                            <Right>
                                <Text>11h ago</Text>
                            </Right>
                        </CardItem>

                        <CardItem>
                            <Text style={{ fontSize: 12 }}>Ada tiang telepon miring yang membahayakan pejalan kaki. mohon untuk segera diperbaiki </Text>
                        </CardItem>

                        {/*<CardItem>
              <Left>
                <Button transparent>
                  <Text>12 Likes</Text>
                </Button>
              </Left>
              <Body>
            
              </Body>
              <Right>
                <Text>11h ago</Text>
              </Right>
            </CardItem>

             <CardItem>
              <Left>
                <Button transparent>
                  <Text>Nama</Text>
                </Button>
              </Left>
              <Body>
               
              </Body>
              <Right>
                <Text>Nama</Text>
              </Right>
            </CardItem>

             <CardItem>
              <Left>
                <Button transparent>
                  <Text>Nik</Text>
                </Button>
              </Left>
              <Body>
                
              </Body>
              <Right>
                <Text>11h ago</Text>
              </Right>
            </CardItem>*/}

                        <ListItem>
                            <Left>
                                <Text style={{ fontSize: 12 }}>Kategori</Text>
                            </Left>

                            <Right>
                                <Text style={{ fontSize: 12 }}>11h ago</Text>
                            </Right>
                        </ListItem>
                        <ListItem>
                            <Left>
                                <Text style={{ fontSize: 12, width: 60 }}>Tag BUMN</Text>
                            </Left>


                            <Label style={{ textAlign: 'center', fontSize: responsiveFontSize(1.5), color: "black", backgroundColor: '#e8f0ff', marginRight: "1%", borderRadius: 2 }}>
                                PT Telekomunikasi Indonesia
                                </Label>

                        </ListItem>
                    </Card>
                    <View>
                        <Picker style={{
                            flex: 1, flexDirection: 'row', marginLeft: "10%", width: "80%", height: 45, borderRadius: 4,
                            borderWidth: 0.5,
                            color: '#000',
                        }}
                            iosHeader="Select one"
                            mode="dropdown"
                            selectedValue={this.state.selected}
                            onValueChange={this.onValueChange3.bind(this)}
                        >
                            <Item label="Menunggu Solusi" value="key0"><Icon name='home' /><Image source={require("../../img/asset/imagedrag.png")} style={{ height: 5, width: 5, flex: 1 }} /></Item>
                            <Item label="Dalam Pengerjaan" value="key1" ></Item>

                        </Picker>
                    </View>

                    
                                <Button bordered light style={{ paddingLeft: "5%", flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: "blue", marginTop: "10%",marginRight:"10%",marginLeft:"10%",width:responsiveWidth(80) }}
                                >
                                    <Image source={require("../../img/asset/ic-send-email.png")}
                                    />
                                    <Text  style={{  }}>Teruskan ke Unit Terkait</Text>
                                </Button>

                                {

                        this.state.status ?

                            <View>

                                <Button bordered light style={{ paddingLeft: "5%",flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: "blue", marginTop: "10%",marginRight:"10%",marginLeft:"10%",width:responsiveWidth(80) }}
                                onPress={() => this.props.navigation.navigate("Photo")}
                                >
                                    <Left>
                                        <Image source={require("../../img/asset/camera.png")}
                                        />
                                    </Left>

                                    <Text style={{ textAlign:"center" }}>Bukti Solusi</Text>



                                </Button>
                            

                    <Button bordered light style={{ borderRadius: 5, paddingLeft: "5%", flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: "blue", marginTop: "2%", marginRight: "10%", marginLeft: "10%", width: responsiveWidth(80) }}
                    onPress={() => this.props.navigation.navigate("DeskripsiEvidance")}>
                        <Left style={{}}>
                            <Image style={{}} source={require("../../img/asset/ic-back.png")} />
                        </Left>


                        <Text style={{ }}>Return Laporan</Text>

                    </Button>
                    </View> : null
                    }

                    <Text style={{ fontSize: responsiveFontSize(1.5), width: "90%", textAlign: "center", marginLeft: "5%" }}>*Anda bisa bagikan pelaporan ini ke tim untuk disolusikan atau dapat mengubah status menjadi on progress apabila pekerjaan ini sedang berlangsung</Text>



                </Content>

            </Container>

        );
    }

    cekstok() {

    }


}

const styles = StyleSheet.create({
    imgFooter: {
        height: "40%",
        width: "22%",
        marginBottom: "5%"
    },
    footer: {
        backgroundColor: "#fff"
    },
    fontFooter: {
        color: "#000",
        fontSize: responsiveFontSize(1.5)
    },
    oneButton: {
        height: 60,
        backgroundColor: '#e41c23',
    },

    oneButtonView: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        height: 20,
        justifyContent: 'center',
    },

    payTextDollar: {
        flex: 1,
        color: '#ffffff',
        fontSize: 20,
        fontFamily: 'MetaOffc-Light',
        textAlign: 'center',
    },

    imageButton: {
        marginTop: 6,
    },
    flexDetailPengerjaan :{
        textAlign: 'center', fontSize: responsiveFontSize(1.5), color: "black", marginLeft: "15%", marginRight: "40%", borderRadius: 2
    }
});

