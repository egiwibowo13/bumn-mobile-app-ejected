import React, { Component } from 'react';
import { Container, Header, Item, Input, Icon, Button, Text } from 'native-base';
import { Alert, TouchableOpacity } from 'react-native';
export default class SearchPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            query: ""
        }
    }
    render() {
        return (
            <Container style={{  }}>
                <Header searchBar rounded style={{ backgroundColor: "#fff" }}>
                    <Item>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate("Beranda")} >
                            <Icon name="arrow-back" style={{ marginRight: "5%", marginTop:"10%" }} />
                        </TouchableOpacity>
                        <Input placeholder="Search" onChangeText={this.handleQuery} />
                        <TouchableOpacity transparent onPress={this.query} >
                            <Icon name="ios-search" />
                        </TouchableOpacity>
                    </Item>
                    <Button transparent onPress={this.query}>
                        <Text>Search</Text>
                    </Button>
                </Header>
            </Container>
        );
    }

    handleQuery = (text) => {
        this.setState({ query: text })
    }

    query = () => {
        if (this.state.query == null || this.state.query == "") {
            Alert.alert(
                'Perhatian',
                'Silahkan isi kolom pencarian',
                [
                    //{text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                    //{text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    { text: 'OK', style: 'cancel' },
                ],
                { cancelable: false }
            )
        } else {
            this.props.navigation.navigate("SearchResult", { query: this.state.query })
        }
    }
}